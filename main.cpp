// ConsoleApplication3.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

class CManager
{
private:
	CManagerFactory   m_factory;
	CManagerInterface *m_manager;
public:
	CManager() : m_factory("mtmanapi.dll"), m_manager(NULL)
	{
		m_factory.WinsockStartup();
		if (m_factory.IsValid() == FALSE || (m_manager = m_factory.Create(ManAPIVersion)) == NULL)
		{
			std::cout << "Failed to create MetaTrader 4 Manager API interface" << std::endl;
			return;
		}
	}
	~CManager()
	{
		if (m_manager != NULL)
		{
			if (m_manager->IsConnected())
				m_manager->Disconnect();
			m_manager->Release();
			m_manager = NULL;
		}
		m_factory.WinsockCleanup();
	}
	bool IsValid()
	{
		return(m_manager != NULL);
	}
	CManagerInterface* operator->()
	{
		return(m_manager);
	}
};

//init mt4 global because i can and have no time for better idea
CManager manager; //  this to close orders
CManager pump_manager; // this to pump

class ParserInit // init class for parsing ini file
{
private:
	boost::property_tree::ptree iniTree;
	std::string m_server;
	int m_login;
	std::string m_password;
	int m_time;
	std::vector<int> m_Logins{ 0 };
	std::string m_path;

public:
	ParserInit(std::string path = "conf.ini") : m_path(path) // we believe that we have Server, Manager, Timer, Logins sections in file
	{
		try
		{		
			boost::property_tree::read_ini(m_path, iniTree);
		
			boost::property_tree::ptree& iniServer = iniTree.get_child("Server");
			boost::property_tree::ptree& iniManager = iniTree.get_child("Manager");
			boost::property_tree::ptree& iniTimer = iniTree.get_child("Timer");
			boost::property_tree::ptree& iniLogins = iniTree.get_child("Logins");

			m_server = iniServer.get<std::string>("adress");
			m_login = iniManager.get<int>("login");
			m_password = iniManager.get<std::string>("password");
			m_time = iniTimer.get<int>("time");

			int counter = iniLogins.size(); // init list of logins opening orders
			m_Logins.resize(counter);
			counter = 0;
			for (auto&i : iniLogins)
			{
				m_Logins[counter] = i.second.get<int>("");
				++counter;
			}
			std::sort(m_Logins.begin(), m_Logins.end());
		}
		catch (boost::property_tree::ini_parser_error)
		{
			std::cout << "No ini file or bad structure" << std::endl << std::endl;
		}
	}
	LPCSTR adress() // ip:port function
	{
		LPCSTR server = m_server.c_str();
		return server;
	}
	int login() // manager login function
	{
		return m_login;
	}
	LPCSTR password() // manager password function
	{
		LPCSTR password = m_password.c_str();
		return password;
	}
	int timer() // closing timer function
	{
		return m_time;
	}
	std::vector<int> logins() // list of logins opening orders function
	{
		return m_Logins;
	}
};

class MTfunctions // class for project functions
{
private:
	static void OrderCloseThread(int *param) // thread for closing orders
	{
		int res = RET_ERROR;
		TradeRecord *trades = NULL;
		TradeTransInfo trans = { 0 };
		int total = 0;
		trades = pump_manager->TradesGet(&total);
		Sleep((*param) * 1000); // this point needs for time interval before closing and because all time is in milisec so we must multiplicate our time counter in 1000
		trans.type = TT_BR_ORDER_CLOSE;
		trans.order = trades[total - 1].order;
		trans.price = trades[total - 1].open_price;
		trans.volume = trades[total - 1].volume;
		if ((res = manager->TradeTransaction(&trans)) != RET_OK)std::cout << "Order closing failed: " << res << manager->ErrorDescription(res) << std::endl;
		else std::cout << "Order " << trans.order << " was closed by programm" << std::endl;
	}
	static void OrderClose(int *param) // function for creation thread for chosen orders
	{
		std::thread close(OrderCloseThread, param);
		close.detach();
	}
public:
	struct m_param // struct for pumping function parametr "param"
	{
		int ms_time;
		std::vector<int> ms_Logins;
	};
	static void ManagerPingThread() // ping function for active connection to MT4Server
	{
		while (TRUE)
		{
			Sleep(180000); //ping every 3 minutes
			manager->Ping();
		}
	}
	static void _stdcall PumpingNotify(int code, int type, void *data, void *param) // pumping callback function, in parametr param we have struct with time and logins list
	{
		if (code == PUMP_UPDATE_TRADES && data != NULL) // check only trades update
		{
			TradeRecord *trades = (TradeRecord*)data; // init trades array
			m_param *tlparam = (m_param*)param; // init param struct
			int *ptimer = &tlparam->ms_time; // exclude time from param struct
			std::vector<int> logins = tlparam->ms_Logins; // exclude logins from param struct
			switch (type)
			{
			case TRANS_ADD: // massage order add and check condition of closing
				
				std::cout << "Created " << trades->order << " order" << std::endl;
				if (std::binary_search(logins.begin(), logins.end(), trades->login))OrderClose(ptimer);
				else std::cout << "It was other user = " << trades->login << std::endl;
				break;
			case TRANS_DELETE: // massage order deleted
				std::cout << "Order " << trades->order << " was closed!" << std::endl;
				break;
			}
		}
	}
};

int main()
{
	int res = RET_ERROR;
	TradeRecord *trades = NULL;
	TradeTransInfo trans = { 0 };
	int total = NULL;
	const std::string fconfig = "conf.ini";

	ParserInit Init(fconfig);
	if (Init.login() == 0)
	{
		system("pause");
		return 10;
	}
	
	MTfunctions::m_param param; // timer and logins gets to pumping as param
	param.ms_time = Init.timer();
	param.ms_Logins = Init.logins();

	MTfunctions::m_param* p_param = &param;

	std::cout << "MetaTrader 4 Manager API: Test project" << std::endl;
	std::cout << "Close opened orders by timer for users" << std::endl;
	std::cout << "Copyright ebanye sobaki s osteohondrozom" << std::endl << std::endl;
	
	if (((res = manager->Connect(Init.adress())) != RET_OK) || ((res = manager->Login(Init.login(), Init.password())) != RET_OK))
	{
		std::cout << "Connect to " << Init.adress() << " as " << Init.login() << " failed " << manager->ErrorDescription(res) << std::endl;
		system("pause");
		return RET_ERROR;
	}
	else
	{
		if (((res = pump_manager->Connect(Init.adress())) != RET_OK) || ((res = pump_manager->Login(Init.login(), Init.password())) != RET_OK))
		{
			std::cout << "Connect to " << Init.adress() << " as " << Init.login() << " failed " << pump_manager->ErrorDescription(res) << std::endl;
			manager->Disconnect();
			system("pause");
			return RET_ERROR;
		}
	}

	std::cout << "Connect to " << Init.adress() << " as " << Init.login() << " successfully" << std::endl;

	if ((res = pump_manager->PumpingSwitchEx(MTfunctions::PumpingNotify, 0, p_param)) != RET_OK)
	{
		std::cout << "Pumping failed" << std::endl;
		system("pause");
		manager->Disconnect();
		pump_manager->Disconnect();
		return RET_ERROR;
	}
	else std::cout << "Pumping switched successfully" << std::endl;


	std::thread manager_ping(MTfunctions::ManagerPingThread);
	manager_ping.detach();

	system("pause");

	manager->Disconnect();
	pump_manager->Disconnect();
	
	return 0;
}