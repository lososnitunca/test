#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "targetver.h"

#include <string.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <windows.h>
#include <thread>
#include <boost/property_tree/ini_parser.hpp>
#include <MT4ManagerAPI.h>

#pragma comment(lib, "Ws2_32.lib")